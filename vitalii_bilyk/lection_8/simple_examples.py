from decorators import decorate, timer, do_twice


@decorate
def simple_func():
    print("Execution of simple function")
    print("rrrr")
    print("tttt")
    print("yyyy")


@timer  # => gen_list_with_append = timer(gen_list_with_append)(n)
def gen_list_with_append(n):
    print('Start function')
    lst = []
    for el in range(n):
        lst.append(el)
    print('End function')
    return lst


@do_twice
def func_sum(p, y, c):
    print("Start and end func_sum")
    """
    Напишите функцию, которая вычисляет сумму
    трех чисел и возвращает результат в основную ветку программы.
    """
    return p + y + c


@do_twice
def say_whee():
    print("Ура!")


def f():
    """
    asdasdasd
    """
    def x():
        print(x)
    print("asdsd")


print(say_whee.__doc__)


#  пишем функцию которая принимает два аргумента
#  пишем декоратор который печатает имя функцкии и все аргументы
