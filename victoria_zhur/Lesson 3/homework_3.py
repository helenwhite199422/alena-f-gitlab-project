import math
import random


"""1. Заменить символ “#” на символ “/” в строке 'www.my_site.com#about'"""

str_1 = "www.my_site.com#about"
print(str_1.replace("#", "/"))


"""2. В строке 'Ivanou Ivan' поменяйте местами слова => 'Ivan Ivanou'"""

str_2 = "Ivanou Ivan"
split_str = str_2.split()  # split of str_2
split_str.reverse()
print(' '.join(split_str))

"""3. Напишите программу которая удаляет пробел в начале строки"""

str_3 = " hello"
print(str_3.lstrip())  # removal of the leading space


"""4. Напишите программу которая удаляет пробел в конце строки"""

str_4 = "hello "
print(str_4.rstrip())  # removal of the trailing space


"""5. a = 10, b=23, поменять значения местами, чтобы в переменную “a”
было записано значение “23”, в “b” - значение “-10”"""

a, b = 10, 23
a, b = b, -a
print("a:", a, "b:", b)


"""6. значение переменной “a” увеличить в 3 раза, а значение “b” уменьшить
на 3"""

a *= 3
b -= 3
print("a:", a, "b:", b)


"""7. преобразовать значение “a” из целочисленного в число с плавающей точкой
(float), а значение в переменной “b” в строку"""

a = float(a)
b = str(b)
print(a, b)


"""8. Разделить значение в переменной “a” на 11 и вывести результат с
точностью 3 знака после запятой"""

print(round(a / 11, 3))


"""9. Преобразовать значение переменной “b” в число с плавающей точкой и
записать в переменную “c”. Возвести полученное число в 3-ю степень."""

c = float(b)
print('Возведение в степень:', c**3)


"""10. Получить случайное число, кратное 3-м"""

d = random.randint(1, 1000) * 3
print(d)


"""11. Получить квадратный корень из 100 и возвести в 4 степень"""

e = math.sqrt(100)**4
print(e)


"""12. Строку “Hi guys” вывести 3 раза и в конце добавить 'Today'"""

str_5 = "Hi guys"
str_6 = "Today"
str_7 = str_5 * 3 + str_6
print(str_7)


"""13. Получить длину строки из предыдущего задания"""

print(len(str_7))  # length of str_7


"""14. Взять предыдущую строку и вывести слово “Today” в прямом и обратном
порядке"""

str_8 = str_7[-5:]  # "Today"
print(str_8)
str_9 = str_8[::-1]  # "yadoT"
print(str_9)


"""15. Hi guysHi guysHi guysToday” вывести каждую 2-ю букву в прямом и
обратном порядке"""

str_10 = str_7.replace(' ', '')
print(str_10[1::2])  # each second symbol in direct order
print(str_10[-2::-2])  # each second symbol in reversed order


"""16. Используя форматирования подставить результаты из задания 10 и 11 в
следующую строку “Task 10: <в прямом>, <в обратном> Task 11: <в прямом>,
<в обратном>”"""

output_10 = str(d)
output_11 = str(e)
print(f"Task 10: {output_10}, {output_10[::-1]} Task 11: {output_11},"
      f" {output_11[::-1]}")


"""17. Есть строка: “my name is name”. Напечатайте ее, но вместо 2ого “name”
вставьте ваше имя."""

str_11 = "my name is name"
split_str_11 = str_11.split(" ")  # split of the string
split_str_11[-1] = "Vika"  # change of the last name to Vika
print(' '.join(split_str_11))


"""18. Полученную строку в задании 12 вывести:"""

print(str_7.title())  # each word starts with capital case
print(str_7.lower())  # each word  starts with lower letter
print(str_7.upper())  # each symbol in upper case


"""19. Посчитать сколько раз слово “Task” встречается в строке из
задания 12"""

print(str_7.count("Task"))
