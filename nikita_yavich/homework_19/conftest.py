from datetime import datetime
import pytest


@pytest.fixture(scope='module', autouse=True)
def file_name_fixture(request):
    # These shows the CONFTEST.PY:
    # print(f"\nTests were started from  {sys.modules[__name__]}")
    # print(f"\nTests were started from  {os.path.abspath(__file__)}")
    # print(f"\nTests were started from  {__file__.split(os.path.sep)[-1]}")
    # print(f"\nTests were started from  {os.path.basename(__file__)}")
    print(f"\nTests were started from  {request.node.name}")


@pytest.fixture(scope='session', autouse=True)
def time_fixture():
    print(f"\nTests were started at {datetime.now()}")
    yield datetime.now()
    print(f"\nTests were finished at {datetime.now()}")


@pytest.fixture(autouse=True)
def fixture_name_fixture(time_fixture, request):
    print(f'\nFixtures used: \n{request.fixturenames}')
    print(
        f'\nTime after session start: '
        f' {datetime.now() - time_fixture}')
