# Task 1. Быки и коровы
# В классическом варианте игра рассчитана на двух игроков.
# Каждый из игроков задумывает и записывает тайное 4-значное число
# с неповторяющимися цифрами. Игрок, который начинает игру по жребию,
# делает первую попытку отгадать число. Попытка — это 4-значное число
# с неповторяющимися цифрами, сообщаемое противнику.
# Противник сообщает в ответ, сколько цифр угадано без совпадения с их
# позициями в тайном числе (то есть количество коров) и сколько угадано
# вплоть до позиции в тайном числе (то есть количество быков).
# При игре против компьютера игрок вводит комбинации одну за другой,
# пока не отгадает всю последовательность.
# Ваша задача реализовать программу,
# против которой можно сыграть в "Быки и коровы"
import string
from random import choice


z = string.digits
x = choice(z[1:10])
for i in range(3):
    z = ''.join(z.split(x[i]))
    x += choice(z)
while True:
    y = input("Введите четырёхзначное число: ")
    bull = 0
    cow = 0
    for i in range(4):
        if x[i] == y[i]:
            bull += 1
        elif y[i] in x:
            cow += 1
    print(y + ' содержит ' + str(bull) + ' быка и ' + str(cow) + ' коровы')
    if bull == 4:
        print('Поздравляю, Ты угадал!!')
        break


# Task 2. Создайте программу, которая, принимая массив имён,
# возвращает строку описывающая количество лайков (как в Facebook).
name_s = input('Введите имена через запятую : ')
list_name_s = name_s.split(',')
if list_name_s[0] == '':
    print('No one likes this')
elif len(list_name_s) == 1:
    print(f'{list_name_s[0]} likes this')
elif len(list_name_s) == 2:
    print(f'{list_name_s[0]} and {list_name_s[1]} like this')
elif len(list_name_s) == 3:
    print(f'{list_name_s[0]} and {list_name_s[1]} and {list_name_s[2]}'
          f' like this')
elif len(list_name_s) > 3:
    print(f'{list_name_s[0]} and {list_name_s[1]} '
          f' and {len(list_name_s) - 2} others like this')
else:
    print(f'{list_name_s[0]}, {list_name_s[1]} and 2 others like this')


# Task 3. Напишите программу,
# которая перебирает последовательность от 1 до 100.
# Для чисел кратных 3 она должна написать: "Fuzz" вместо печати числа,
# а для чисел кратных 5  печатать "Buzz".
# Для чисел которые кратны 3 и 5 надо печатать "FuzzBuzz".
# Иначе печатать число.
list_of_numbers = list(range(1, 101))
for i in list_of_numbers:
    if i % 3 == 0 and i % 5 == 0:
        print('FuzzBuzz')
    elif i % 3 == 0:
        print('Fuzz')
    elif i % 5 == 0:
        print('Buzz')
    else:
        print(i)


# Task 4. Напишите код, который возьмет список строк и пронумерует их.
# Нумерация начинается с 1, имеет “:” и пробел
# [] => []
# ["a", "b", "c"] => ["1: a", "2: b", "3: c"]
lst_1 = list(map(str, input('Введите Ваш список: ').split()))
lst_2 = [str(lst_1.index(i) + 1) + ': ' + i for i in lst_1]
print('Task 4. : ', lst_2)


# Task 5. Проверить, все ли элементы одинаковые
# [1, 1, 1] == True
# [1, 2, 1] == False
# ['a', 'a', 'a'] == True
# [] == True
# Вариант ввода с пробелом (но с [] == True затык у меня)
your_list = input('Введи свой список через пробел : ')
s = your_list.split()
print('Task 5. : ', len(s) == s.count(s[0]))


# Task 6. : Проверка строки. В данной подстроке проверить все ли буквы
# в строчном регистре или нет и вернуть список не подходящих.
# dogcat => [True, []]
# doGCat => [False, ['G', 'C']]
my_srt_in = str(input('Введите строку для проверки: '))
my_out = [True, []]
for m in my_srt_in:
    if m.isupper():
        my_out[0] = False
        my_out[1].append(m)
print(my_out)


# Task 7. Сложите все числа в списке, они могут быть отрицательными,
# если список пустой вернуть 0
# [] == 0
# [1, 2, 3] == 6
# [1.1, 2.2, 3.3] == 6.6
# [4, 5, 6] == 15
# range(101) == 5050
my_list = list()
if not my_list:
    print(0)
else:
    print(sum(my_list))
