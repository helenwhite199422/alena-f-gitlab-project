"""
1.Validate
Ваша задача написать программу, принимающее число - номер кредитной
карты(число может быть четным или не четным). И проверяющей может ли такая
карта существовать. Предусмотреть защиту от ввода букв, пустой строки и т.д.
Примечания
Алгоритм Луна
Примеры
validate(4561261212345464) #=> False
validate(4561261212345467) #=> True
Для проверки:
https://www.paypalobjects.com/en_GB/vhelp/paypalmanager_help/
credit_card_numbers.htm
"""
# from functools import reduce


# ввод с защитой от пробелов, можно вводить с пробелом и без
code = input("Enter code: ").replace(" ", "")


def luhn(code: str):
    """
    This function validation current card number
    """
    # защита от пустой строки, просит заново ввести код
    while len(code) == 0:
        code = input("code is empty, enter code: ").replace(" ", "")
    # защита от букв, просит заново ввести код
    while code.isdigit() is False:
        code = input("code is not correct,"
                     " enter correct code: ").replace(" ", "")
    LOOKUP = (0, 2, 4, 6, 8, 1, 3, 5, 7, 9)
    # эта часть кода из вики не выполняет полную защиту
    # code = reduce(str.__add__, filter(str.isdigit, code))
    evens = sum(int(i) for i in code[-1::-2])
    odds = sum(LOOKUP[int(i)] for i in code[-2::-2])
    return ((evens + odds) % 10 == 0)


print("validation: ", luhn(code))
print(luhn.__doc__)
print(luhn.__annotations__)


"""
2. Подсчет количества букв
На вход подается строка, например, "cccbba" результат работы программы
- строка “c3b2a"

Примеры для проверки работоспособности:
"cccbba" == "c3b2a"
"abeehhhhhccced" == "abe2h5c3ed"
"aaabbceedd" == "a3b2ce2d2"
"abcde" == "abcde"
"aaabbdefffff" == "a3b2def5"
"""
str1 = input("enter string: ")


def count(str1: str):
    """
    function counts
    """
    x = []
    for i in str1:
        y = i + str(str1.count(i))
        if y not in x:
            x.append(y)
    x = ''.join(x)
    return x


print(count(str1))
print(count.__doc__)
print(count.__annotations__)


"""
3. Простейший калькулятор v0.1
Реализуйте программу, которая спрашивала у пользователя, какую операцию он
хочет произвести над числами, а затем запрашивает два числа и выводит результат
Проверка деления на 0.

Пример
Выберите операцию:
    1. Сложение
    2. Вычитание
    3. Умножение
    4. Деление
Введите номер пункта меню:
>>> 4
Введите первое число:
>>> 10
Введите второе число:
>>> 3
Частное: 3, Остаток: 3
"""
cal_var = int(input("Выберите № пункта: 1. Сложение 2. Вычитание "
                    "3. Умножение 4. Деление : "))


def calc(cal_var: int):
    """
    Calculator work
    """
    x = int(input("x = "))
    y = int(input("y = "))
    if cal_var == 1:
        return x + y
    elif cal_var == 2:
        return x - y
    elif cal_var == 3:
        return x * y
    elif cal_var == 4:
        if y == 0:
            return 0
        return x / y


print(round(calc(cal_var), 1))
print(calc.__doc__)
print(calc.__annotations__)


"""
4. Написать функцию с изменяемым числом входных параметров
При объявлении функции предусмотреть один позиционный и один
именованный аргумент, который по умолчанию равен None (в примере это
аргумент с именем name).
Также предусмотреть возможность передачи нескольких именованных и позиционных
аргументов
Функция должна возвращать следующее
result = function(1, 2, 3, name=’test’, surname=’test2’, some=’something’)
Print(result)
🡪 {“mandatory_position_argument”: 1, “additional_position_arguments”: (2, 3),
      “mandatory_named_argument”: {“name”: “test2”},
       “additional_named_arguments”:
              {“surname”: “test2”, “some”: “something”}}
"""


def my_function(x, *args, name=None, **kwargs):
    """
    function argument
    """
    return {"mandatory_position_argument": x,
            "additional_position_arguments": args,
            "mandatory_named_argument": {"name": name},
            "additional_named_arguments": {**kwargs}
            }


result = my_function(1, 2, 3, name="test", surname="test2", some="something")
print(result)
print(my_function.__doc__)
print(my_function.__annotations__)


"""
5. Работа с областями видимости
На уровне модуля создать список из 3-х элементов
Написать функцию, которая принимает на вход этот список и добавляет в него
элементы. Функция должна вернуть измененный список.
При этом исходный список не должен измениться. Пример c функцией которая
добавляет в список символ “a”:
My_list = [1, 2, 3]
Changed_list = change_list(My_list)
Print(My_list)
🡪 [1, 2, 3]
Print(Changed_list)
🡪 [1, 2, 3, “a”]
"""
my_list = [1, 2, 3]


def elem(my_list: list):
    """
    add element for list
    """
    my_list2 = my_list + [input("enter element: ")]
    return my_list2


print(elem(my_list))
print(elem.__doc__)
print(elem.__annotations__)


"""
6. Функция проверяющая тип данных
Написать функцию которая принимает на фход список из чисел, строк и таплов
Функция должна вернуть сколько в списке элементов приведенных данных
print(my_function([1, 2, “a”, (1, 2), “b”])
🡪 {“int”: 2, “str”: 2, “tuple”: 1}
"""
list_a = [1, 2, 'a', (1, 2), 'b']


def type_count(list_a: list):
    """
    function type and count
    """
    a = 0
    b = 0
    c = 0
    for i in range(len(list_a)):
        if isinstance(list_a[i], int):
            a += 1
        if isinstance(list_a[i], str):
            b += 1
        if isinstance(list_a[i], tuple):
            c += 1
    type_dict = {'int': a, 'str': b, 'tuple': c}
    return type_dict


print(type_count(list_a))
print(type_count.__doc__)
print(type_count.__annotations__)


"""
7. Написать пример чтобы hash от объекта 1 и 2 были одинаковые, а id разные.
"""
# не работает
a = 100000000
b = 100000000

print(id(a), id(b))
print(hash(a), hash(b))

"""
8. написать функцию, которая проверяет есть ли в списке объект,
которые можно вызвать
"""


def call_list(a: list):
    """
    call a
    """
    return [callable(call) for call in a]


a = [1, 2, 3, call_list]
print(call_list(a))
print(call_list.__doc__)
print(call_list.__annotations__)
