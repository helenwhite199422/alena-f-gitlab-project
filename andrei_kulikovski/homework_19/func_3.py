import string


def detect_special_symbl(arg):
    for char in arg:
        if char in string.punctuation:
            return True
        return False


# print(detect_special_symbl("%"))
# print(detect_special_symbl("t"))
