from andrey_viatoshkin.home_work_24.pages.main_page import MainPage


def test_tab_name(driver):
    main_page = MainPage(driver)
    main_page.open_site()
    assert 'Oscar - Sandbox' == main_page.get_title(), 'Wrong tab'


def test_page_url(driver):
    main_page = MainPage(driver)
    main_page.open_site()
    assert 'http://selenium1py.pythonanywhere.com/en-gb/' ==\
           main_page.get_page_url(), 'Wrong URL'
