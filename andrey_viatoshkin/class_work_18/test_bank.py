import unittest


from tms.andrey_viatoshkin.home_work_7.task_1.\
    home_work_7_task_1 import bank


class TestBank(unittest.TestCase):

    def test_deposit_sum_positive(self):
        self.assertEqual(bank(20, 10), 51.87484920199999)

    def test_deposit_sum_with_zero_years(self):
        self.assertEqual(bank(20, 0), 20)

    def test_deposit_sum_with_zero_deposit(self):
        self.assertEqual(bank(0, 10), 0)

    @unittest.expectedFailure
    def test_deposit_sum_with_none_deposit(self):
        self.assertEqual(bank(None, 10), 0)

    @unittest.expectedFailure
    def test_deposit_sum_with_none_year(self):
        self.assertEqual(bank(10, None), 0)

    @unittest.expectedFailure
    def test_deposit_sum_with_string_deposit(self):
        self.assertEqual(bank('string', 10), 0)

    @unittest.expectedFailure
    def test_deposit_sum_with_string_year(self):
        self.assertEqual(bank(20, '10'), 0)

    @unittest.expectedFailure
    def test_deposit_sum_negative(self):
        self.assertNotEqual(bank(20, 10), 51.87484920199999)
