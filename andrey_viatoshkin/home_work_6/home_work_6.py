# 1.Validate
# Ваша задача написать программу, принимающее число - номер кредитной
# карты(число может быть четным или не четным). И проверяющей может ли
# такая карта существовать. Предусмотреть защиту от ввода букв,
# пустой строки и т.д.
# Примечания
# Алгоритм Луна
# Примеры
# validate(4561261212345464) #=> False
# validate(4561261212345467) #=> True

def validate(int_1=None):
    '''
    Allows to check if card number is valid or not
    '''
    if isinstance(int_1, int):
        int_1_list = list(map(int, str(int_1)))
        if len(int_1_list) % 2 == 0:
            each_second_number = int_1_list[len(int_1_list) - 2::-2]
            each_first_number = int_1_list[1::2]
        else:
            each_second_number = int_1_list[len(int_1_list) - 1::-2]
            each_first_number = int_1_list[1::2]
        list_for_check = []
        for i in each_second_number:
            if i * 2 > 9:
                list_for_check.append(i * 2 - 9)
            else:
                list_for_check.append(i * 2)
        result_sum = sum(list_for_check) + \
            sum(each_first_number)
        result_sum_bool = result_sum % 10 == 0
        print(result_sum_bool)
    elif isinstance(int_1, str):
        print('Введите цифры')
    else:
        print('Введите значение')


validate(4561261212345467)


# 2. Подсчет количества букв
# На вход подается строка, например, "cccbba" результат работы программы
# - строка “c3b2a"

def count_letters(str_1: str):
    '''
    Count the same characters in a row
    '''
    list_of_letters = []
    for i in str_1:
        count_of_letters = i + str(str_1.count(i))
        if count_of_letters not in list_of_letters:
            list_of_letters.append(count_of_letters)
    list_of_letters = ''.join(list_of_letters).replace('1', '')
    return list_of_letters


print(count_letters('cccbba'))


# 3. Простейший калькулятор v0.1
# Реализуйте программу, которая спрашивала у пользователя, какую операцию
# он хочет произвести над числами, а затем запрашивает два числа и
# выводит результат
# Проверка деления на 0.
#
# Пример
# Выберите операцию:
#     1. Сложение
#     2. Вычитание
#     3. Умножение
#     4. Деление
# Введите номер пункта меню:
# >>> 4
# Введите первое число:
# >>> 10
# Введите второе число:
# >>> 3
# Частное: 3, Остаток: 3


def calculator():
    '''
    Calculator
    '''
    select_option = int(input('Выберите операцию:\n 1. Сложение\n 2. '
                              'Вычитание\n 3. Умножение\n 4. Деление'
                              '\nВведите номер пункта меню: '))
    print(select_option)
    first_number = int(input('Введите первое число: '))
    second_number = int(input('Введите второе число: '))
    if select_option == 1:
        print(first_number + second_number)
    if select_option == 2:
        print(first_number - second_number)
    if select_option == 3:
        print(first_number * second_number)
    if select_option == 4:
        if first_number % second_number == 0:
            print(int(first_number / second_number))
        else:
            print('Частное:', int(first_number / second_number),
                  ', Остаток:', first_number % second_number)


# calculator()

# 4. Написать функцию с изменяемым числом входных параметров
# При объявлении функции предусмотреть один позиционный и один именованный
# аргумент, который по умолчанию равен None (в примере это аргумент с
# именем name). Также предусмотреть возможность передачи нескольких
# именованных и позиционных аргументов
def function(b: str, *args, name=None, **kwargs):
    '''
    Display arguments
    '''
    return ('mandatory_position_argument:', b,
            'additional_position_arguments', args,
            'mandatory_named_argument:name:', name,
            'additional_named_arguments', kwargs)


result = function(1, 2, 3, name='test', surname='test2', some='something')

print(result)


# 5. Работа с областями видимости
# На уровне модуля создать список из 3-х элементов
# Написать функцию, которая принимает на вход этот список и добавляет в него
# элементы. Функция должна вернуть измененный список.
# При этом исходный список не должен измениться.
my_list = [1, 2, 3]


def append_a(my_list: list):
    '''
    modify list without impacting parent list
    '''
    change_list = my_list[:]
    change_list.append('a')
    return change_list


change_list = append_a(my_list)
print(change_list)
print(my_list)


# 6. Функция проверяющая тип данных
# Написать функцию которая принимает на фход список из чисел, строк и таплов
# Функция должна вернуть сколько в списке элементов приведенных данных


def count_type_of_parameters(*args):
    '''
    Count each type of data
    '''
    list_of_parameters = [*args]
    int_elements = sum(isinstance(x, int) for x in list_of_parameters)
    str_elements = sum(isinstance(x, str) for x in list_of_parameters)
    tuple_elements = sum(isinstance(x, tuple) for x in list_of_parameters)
    list_elements = sum(isinstance(x, list) for x in list_of_parameters)
    return int_elements, str_elements, tuple_elements, list_elements


test_count = count_type_of_parameters(1, 2, 'a', (1, 2), 'b', [123])
print(test_count)


# 7. Написать пример чтобы hash от объекта 1 и 2 были одинаковые,
# а id разные.

print(hash(1))
print(hash(1.0))
print(id(1))
print(id(1.0))

# 8. написать функцию, которая проверяет есть ли в списке объект, которые
# можно вызвать


def is_callable(test_list: list):
    '''
    Check if element of the list is callable
    '''
    for element in test_list:
        print(callable(element))


is_callable([str, 2, 3, int, 4, 5])

lis_1 = []
str_1 = 'lissssddsds'
