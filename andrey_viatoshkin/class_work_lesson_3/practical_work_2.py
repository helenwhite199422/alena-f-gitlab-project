# 1. Свяжите переменную с любой строкой, состоящей не менее чем из 8
# символов. Извлеките из строки первый символ, затем последний, третий с
# начала и третий с конца. Измерьте длину вашей строки.


str1 = 'sdasd ds das d  fdfsfd'


print(str1[0])
print(str1[- 1])
print(str1[2])
print('третий символ с конца ' + str1[-3])
print(len(str1))

# 2. Присвойте произвольную строку длиной 10-15 символов переменной и
# извлеките из нее следующие срезы:
# первые восемь символов
# четыре символа из центра строки
# символы с индексами кратными трем
# переверните строку

str2 = 'dad sd sdsadsa dsd sd sdsadasds dsdasdsds asd5'
str_length = len(str2)
avg_symbol = int(len(str2) / 2)
next_required_symbol = int(avg_symbol + 4)

print(str_length)
print('первые восемь символов ' + str2[0:8])
print(str2[avg_symbol: next_required_symbol])
print(str2[::3])
print(str2[::-1])
print()
print()
# 3. Есть строка: “my name is name”. Напечатайте ее, но вместо 2ого “name”
# вставьте ваше имя.

sentence = 'my name is name'
name = 'name'
my_name = 'Andrey'
replace_word_name = sentence.replace('name', my_name)
replace_word_Andrey = replace_word_name.replace('Andrey', name, 1)

print(replace_word_Andrey)

# 4. Есть строка: test_tring = "Hello world!", необходимо

test_string = "Hello world!"
print(test_string.find('w'))
print(test_string.count('l'))
print(test_string.startswith('Hello'))
print(test_string.endswith('qwe'))
