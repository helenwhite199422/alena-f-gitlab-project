import math
import random


"""Task_3_1: Заменить символ “#” на символ “/”
 в строке 'www.my_site.com#about'. """
test_website_string = 'www.my_site.com#about'
test_website_string = test_website_string.replace('#', '/')
print(test_website_string)


"""Task_3_2: В строке “Ivanou Ivan” поменяйте
местами слова => "Ivan Ivanou". """
# первый способ через join()
test_string = "Ivan Ivanou"
new_test_string = ' '.join(test_string.split(' ')[::-1])
print(new_test_string)

# второй способ через reverse()
reverse_string = test_string.split()
reverse_string.reverse()
print(*reverse_string)


"""Task_3_3: Напишите программу которая удаляет пробел в начале строки. """
test_string_2 = "   Ivan Ivanou   "
new_test_string_2 = test_string_2.lstrip()  # можно сделать через input().
print(new_test_string_2)


"""Task_3_4: Напишите программу которая удаляет пробел в конце строки. """
test_string_3 = "   Ivan Ivanou   "
new_test_string_3 = test_string_3.rstrip()  # можно сделать через input().
print(new_test_string_3)


"""Task_3_5: a = 10, b = 23, поменять значения местами, чтобы в переменную “a”
было записано значение “23”, в “b” - значение “10”. """
a, b = 10, 23
a, b = b, a
print(a, b, sep='\n')


"""Task_3_6: значение переменной “a” увеличить в 3 раза,
 а значение “b” уменьшить на 3. """
a *= 3
b -= 3
print(a, b, sep="\n")


"""Task_3_7: преобразовать значение “a” из целочисленного в число с плавающей
точкой (float), а значение в переменной “b” в строку. """
a = float(a)
b = str(b)
print(a, b, sep='\n')


"""Task_3_8: Разделить значение в переменной “a” на 11
и вывести результат с точностью 3 знака после запятой. """
a = round(a / 11, 3)
print(a)


"""Task_3_9: Преобразовать значение переменной “b” в число с плавающей точкой
и записать в переменную “c”. Возвести полученное число в 3-ю степень. """
c = pow(float(b), 3)
print(c)


"""Task_3_10: Получить случайное число, кратное 3-м. """
random_number = random.randrange(3, 1000, 3)
print('Number:', random_number)


"""Task_3_11: Получить квадратный корень из 100 и возвести в 4 степень. " """
num = pow(math.sqrt(100), 4)
print(num)


"""Task_3_12: Строку “Hi guys” вывести 3 раза и в конце добавить “Today”
“Hi guysHi guysHi guysToday”. """
test_str = 'Hi guys' * 3 + 'Today'
print(test_str)


"""Task_3_13: Получить длину строки из предыдущего задания. """
len_test_str = len(test_str)
print(len_test_str)


"""Task_3_14: Взять предыдущую строку и вывести слово “Today”
в прямом и обратном порядке. """
today_str = test_str[-5:]
print(today_str)
print(today_str[::-1])


"""Task_3_15: “Hi guysHi guysHi guysToday” вывести каждую 2-ю букву
 в прямом и обратном порядке. """
test_str = test_str.replace(' ', '')
second_letter = test_str[1::2]
revers_second_letter = test_str[-2::-2]
print(second_letter, revers_second_letter, sep='\n')


"""Task_3_16: Используя форматирования подставить результаты из задания
10 и 11 в следующую строку:
 “Task 10: <в прямом>,<в обратном> Task 11: <в прямом>, <в обратном>”. """
result_task_3_10 = str(random_number)
result_task_3_11 = str(num)
result_1 = f'Task 10: {result_task_3_10}, {result_task_3_10[::-1]}'
result_2 = f' Task 11: {result_task_3_11}, {result_task_3_11[::-1]}'
result_task_3_16 = result_1 + ' ' + result_2
print(result_task_3_16)


"""Task_3_17: Есть строка: “my name is name”.
Напечатайте ее, но вместо 2ого “name” вставьте ваше имя."""
my_name = 'my name is name'
my_name = my_name.replace('name', 'Kostya')
my_name = my_name.replace('Kostya', 'name', 1)
print(my_name)


"""Task_3_18: Полученную строку в задании 12 вывести:
а) Каждое слово с большой буквы
б) все слова в нижнем регистре
в) все слова в верхнем регистре. """
test_string_title = test_str.title()
test_string_lower = test_str.lower()
test_string_upper = test_str.upper()
print(test_string_title, test_string_lower, test_string_upper, sep='\n')


"""Task_3_19: Посчитать сколько раз слово “Task”
встречается в строке из задания 12. """
count_task = test_str.count('Task')
print(count_task)
