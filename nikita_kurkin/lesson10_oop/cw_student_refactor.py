"""
Путешествие
Вы идете в путешествие, надо подсчитать сколько у денег у каждого
студента. Класс студен описан следующим образом:

class Student:
    def __init__(self, name, money):
        self.name = name
        self.money = money

Необходимо понять у кого больше всего денег и вернуть его имя. Если
у студентов денег поровну вернуть: “all”.
(P.S. метод подсчета не должен быть частью класса)


assert most_money([ivan, oleg, sergei]) == "all"
assert most_money([ivan, oleg]) == "ivan"
assert most_money([oleg]) == "Oleg"
"""


class Student:
    def __init__(self, name, money=None):
        self.name = name
        self._money = money

    @property
    def mmoney(self):
        return self._money

    @mmoney.setter
    def mmoney(self, value):
        self._money = value

    @mmoney.deleter
    def mmoney(self):
        self._money = None


def most_money_1(lst: list):
    money_lst = [i.money for i in lst]
    max_value = max(money_lst)
    if money_lst.count(max_value) == len(money_lst) and len(money_lst) > 1:
        return 'all'
    else:
        for person in lst:
            if person.money == max_value:
                return person.name


if __name__ == '__main__':
    ivan = Student('Ivan', 1000)
    ivan1 = Student('Ivan')
    ivan1.money = 1111
    oleg = Student('Oleg', 2000)
    sergei = Student('Sergei', 3000)
    alexey = Student('Alexey', 3000)
    print(most_money_1([ivan, oleg, sergei]))
    print(most_money_1([ivan, oleg]))
    print(most_money_1([oleg, ivan1]))
    # assert most_money([ivan, oleg, sergei]) == "all"
    # assert most_money_1([sergei, alexey]) == "all"
    # assert most_money_1([ivan, oleg]) == "Ivan"
    # assert most_money_1([oleg]) == "Oleg"
