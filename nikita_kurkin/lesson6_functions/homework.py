"""
Ваша задача написать программу, принимающее число -
номер кредитной карты(число может быть четным или не четным).
И проверяющей может ли такая карта существовать.
Предусмотреть защиту от ввода букв, пустой строки и т.д.
"""


def validate(credit_card_number) -> bool:
    credit_card_number = [int(i) for i in str(credit_card_number)]
    some_list = []
    sum_ = 0
    for i in range(len(credit_card_number)):
        if i % 2 == 0:
            digit = credit_card_number[i] * 2
            if digit > 9:
                digit -= 9
            some_list.append(digit)
        else:
            some_list.append(credit_card_number[i])

    for j in some_list:
        sum_ += j
    if sum_ % 10 == 0:
        return True
    else:
        return False


print(validate(4561261212345464))  # => False
print(validate(4561261212345467))  # => True

'''
2. Подсчет количества букв
На вход подается строка, например, "cccbba" результат
работы программы - строка “c3b2a"

Примеры для проверки работоспособности:
"cccbba" == "c3b2a"
"abeehhhhhccced" == "abe2h5c3ed"
"aaabbceedd" == "a3b2ce2d2"
"abcde" == "abcde"
"aaabbdefffff" == "a3b2def5"
'''


def count_of_same_letter(some_string: str) -> str:
    list_of_letters = [i for i in some_string]
    new_string = ''
    counter = 1
    for i in range(len(list_of_letters) - 1):
        new_counter = 1
        if list_of_letters[i] == list_of_letters[i + 1]:
            counter += 1
        else:
            if counter == 1:
                new_string += list_of_letters[i]
                counter = new_counter
            else:
                new_string += (list_of_letters[i] + str(counter))
                counter = new_counter

    return new_string


print(count_of_same_letter("cccbba"))
print(count_of_same_letter("abeehhhhhccced"))
print(count_of_same_letter("aaabbceedd"))
print(count_of_same_letter("aaabbdefffff"))

'''
Простейший калькулятор v0.1
Реализуйте программу, которая спрашивала у пользователя,
какую операцию он хочет произвести над числами, а затем
запрашивает два числа и выводит результат
Проверка деления на 0.
'''


def calculation():
    operation = input("Enter your operation with numbers: ")
    digit_1 = int(input("Enter your first digit: "))
    digit_2 = int(input("Enter your second digit: "))
    if operation == '/':
        return f'{digit_1 // digit_2} целое, {digit_1 % digit_2} остаток'
    elif operation == '*':
        return digit_2 * digit_1
    elif operation == '+':
        return digit_1 + digit_2
    elif operation == '-':
        return digit_1 - digit_2
    else:
        return 'Incorrect operation!'


print(calculation())


"""
Написать функцию с изменяемым числом входных параметров

При объявлении функции предусмотреть один позиционный и
один именованный аргумент, который по умолчанию равен None
(в примере это аргумент с именем name).
Также предусмотреть возможность передачи нескольких именованных
и позиционных аргументов

Функция должна возвращать следующее
result = function(1, 2, 3, name=’test’, surname=’test2’, some=’something’)
Print(result)


🡪 {“mandatory_position_argument”: 1,
“additional_position_arguments”: (2, 3),
      “mandatory_named_argument”: {“name”: “test2”},
      “additional_named_arguments”:
      {“surname”: “test2”, “some”: “something”}}
"""


def some_function(a1, b1, c1, name=None, **kwargs):
    some_dict = {
        "mandatory_position_argument": a1,
        "additional_position_arguments": (b1, c1),
        "mandatory_named_argument": {"name": name},
        "additional_named_arguments": kwargs
    }
    return some_dict


print(some_function(1, 2, 3, name='test', surname='test2',
                    some='something'))

"""
Работа с областями видимости

На уровне модуля создать список из 3-х элементов
Написать функцию, которая принимает на вход этот список и
добавляет в него элементы. Функция должна вернуть измененный список.
При этом исходный список не должен измениться. Пример c функцией
которая добавляет в список символ “a”:

My_list = [1, 2, 3]
Changed_list = change_list(My_list)
Print(My_list)
🡪 [1, 2, 3]

Print(Changed_list)
🡪 [1, 2, 3, “a”]
"""

random_list_with_numbers = [1, 2, 3, 4]


def change_list(some_list):
    new_list = some_list
    new_list.append(777)
    return new_list


print(random_list_with_numbers)
print(change_list(random_list_with_numbers))


"""
Функция проверяющая тип данных
Написать функцию которая принимает на вход список из чисел, строк
и таплов.Функция должна вернуть сколько в списке элементов
приведенных данных

print(my_function([1, 2, “a”, (1, 2), “b”])

🡪 {“int”: 2, “str”: 2, “tuple”: 1}
"""


def what_is_type_of_this(some_list):
    count_int, count_str, count_tuple = 0, 0, 0
    for i in some_list:
        if isinstance(i, str):
            count_str += 1
        elif isinstance(i, int):
            count_int += 1
        elif isinstance(i, tuple):
            count_tuple += 1

    str_ = {
        "int": count_int if count_int > 0 else 0,
        "str": count_str if count_str > 0 else 0,
        "tuple": count_tuple if count_tuple > 0 else 0
    }

    return str_


print(what_is_type_of_this([1, 2, 'a', (1, 2), 'b']))

"""
Написать пример чтобы hash от объекта 1 и 2 были одинаковые,
а id разные.
"""
a = 1000
b = --a
print(hash(a), hash(b))
print(id(a), id(b))


"""
написать функцию, которая проверяет есть ли в списке объект,
которые можно вызвать
"""


def check_callable(some_object):
    for i in some_object:
        if not callable(i):
            pass
        else:
            return i


print(check_callable([1, int]))
