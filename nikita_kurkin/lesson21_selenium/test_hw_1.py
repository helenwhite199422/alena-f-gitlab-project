from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.by import By


def test_checkbox_heroku(browser):
    browser.get('http://the-internet.herokuapp.com/')
    browser.find_element(By.CSS_SELECTOR, 'a[href*="/checkboxes"]').click()
    check_1 = browser.find_element(By.XPATH, '//input[@type="checkbox"][1]')
    check_1.click()
    check_2 = browser.find_element(By.XPATH, '//input[@type="checkbox"][2]')
    check_2.click()
    assert not check_2.is_selected(), "Checkbox is selected"
    assert check_1.is_selected(), "Checkbox is not selected"


def test_multiple_window(browser):
    browser.get('http://the-internet.herokuapp.com/')
    browser.find_element(By.CSS_SELECTOR, 'a[href*="/window"]').click()
    browser.find_element(By.CSS_SELECTOR, 'a[href*="/new"]').click()
    browser.switch_to.window(browser.window_handles[1])
    title = browser.title
    assert title == "New Window", "Title is not the same"


def test_add_or_remove_elements(browser):
    browser.get('http://the-internet.herokuapp.com/')
    browser.find_element(By.CSS_SELECTOR,
                         'a[href*="/add_remove_elements"]').click()
    browser.find_element(By.XPATH, '//div/button').click()
    delete_button = browser.\
        find_element(By.XPATH, '//button[@onclick="deleteElement()"]')
    assert delete_button.is_enabled(), "Button is available for clicking"


def test_additional_for_check_delete_button(browser):
    browser.get('http://the-internet.herokuapp.com/add_remove_elements/')
    browser.find_element(By.XPATH, '//div/button').click()
    try:
        delete_button = browser.\
            find_element(By.CSS_SELECTOR, 'button:first-child.added-manually')
        delete_button.click()
        assert not delete_button.is_displayed()
    except StaleElementReferenceException:
        print('\nElement is not available')
