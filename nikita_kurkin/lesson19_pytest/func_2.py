class InputTypeError(Exception):
    message = 'Wrong type of input value'

    def __init__(self):
        super().__init__(self.message)


def reverse_to_float(digit):
    if isinstance(digit, str):
        raise InputTypeError
    return float(digit)
