import unittest
import book
from unittest.mock import patch


class TestPrice(unittest.TestCase):

    def test_create_book_with_valid_price(self):
        book1 = book.Book("Elena", 2010, 500, 100)
        self.assertEqual(book1.price, 100)

    def test_create_book_with_invalid_lower_price(self):
        with self.assertRaises(book.PriceException):
            book.Book("Elena", 2010, 500, 99)

    def test_create_book_with_invalid_bigger_price(self):
        with self.assertRaises(book.PriceException):
            book.Book("Elena", 2010, 500, 10001)

    def test_type_error_message_price(self):
        with self.assertRaises(book.PriceException) as context:
            book.Book("Elena", 2010, 500, 99)

        self.assertTrue('Price should be in range 100 - 10000' in
                        str(context.exception))

    @patch('book.Book.validate_price', return_value=100)
    def test_mock_price(self, arg):
        book11 = book.Book("Elena", 2010, 500, 88)
        self.assertEqual(book11.price, 100)


class TestPages(unittest.TestCase):

    def test_create_book_with_valid_pages(self):
        book2 = book.Book("Elena", 2010, 3999, 100)
        self.assertEqual(book2.pages, 3999)

    def test_create_book_with_invalid_pages(self):
        with self.assertRaises(book.PagesException):
            book.Book("Elena", 2010, 4000, 10000)

    def test_type_error_message_pages(self):
        with self.assertRaises(book.PagesException) as context:
            book.Book("Elena", 2010, 4000, 100)

        self.assertTrue('Number of pages should be less than 4000' in
                        str(context.exception))


class TestYear(unittest.TestCase):

    def test_create_book_with_valid_year(self):
        book3 = book.Book("Elena", 1981, 3999, 100)
        self.assertEqual(book3.year, 1981)

    def test_create_book_with_invalid_year(self):
        with self.assertRaises(book.YearException):
            book.Book("Elena", 1980, 4000, 10000)

    def test_type_error_message_year(self):
        with self.assertRaises(book.YearException) as context:
            book.Book("Elena", 1980, 4000, 100)

        self.assertTrue('Year should be more than 1980' in
                        str(context.exception))


class TestAuthor(unittest.TestCase):

    def test_create_book_with_valid_author(self):
        book4 = book.Book("Elena", 1981, 3999, 100)
        self.assertEqual(book4.author, "Elena")

    def test_create_book_with_digit_author(self):
        with self.assertRaises(book.AuthorException):
            book.Book("Elena1", 1980, 4000, 10000)

    def test_create_book_with_symbol_author(self):
        with self.assertRaises(book.AuthorException):
            book.Book("@Elena", 1980, 4000, 10000)

    def test_type_error_message_author(self):
        with self.assertRaises(book.AuthorException) as context:
            book.Book("Elena1", 1980, 4000, 100)

        self.assertTrue('Author can contain only letters' in
                        str(context.exception))
