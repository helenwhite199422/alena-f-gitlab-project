from bank_deposit import calculate_deposit
import unittest


class TestDepositPositiveChecks(unittest.TestCase):
    def test_1(self):
        self.assertEqual(calculate_deposit(1000, 5), 1610)

    def test_2(self):
        self.assertEqual(calculate_deposit(1000, 0), 1000)

    def test_result_type(self):
        self.assertIsInstance(calculate_deposit(500, 3), int)


class TestDepositNegativeChecks(unittest.TestCase):

    def test_type_error_deposit(self):
        with self.assertRaises(TypeError):
            calculate_deposit('e', 4)

    def test_type_error_year(self):
        with self.assertRaises(TypeError):
            calculate_deposit(45200, [1, 2])
