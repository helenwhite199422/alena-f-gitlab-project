def convert_to_float(arg):
    """2ая – переводит переданное число в тип float, если передана строка
    вызывает пользовательскую ошибку InputTypeError (создать ее самому)"""
    if isinstance(arg, str):
        raise TypeError("It is a string.. cannot convert!")
    return float(arg)
