from collections import Counter

# Task1: Перевести строку в массив
# "Robin Singh" => ["Robin”, “Singh"]
# "I love arrays they are my favorite" => ["I", "love", "arrays", "they",
# "are", "my", "favorite"]
str1 = 'I love arrays they are my favorite'
print('Task 1:', str1.split())

# Task2: Дан список: [‘Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus
# Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”
lst1 = ['Ivan', 'Ivanou']
str2 = 'Minsk'
str3 = 'Belarus'
print(f'Task 2: Привет, {" ".join(lst1)}! Добро пожаловать в {str2} {str3}')

# Task3: Дан список ["I", "love", "arrays", "they", "are", "my", "favorite"]
# сделайте из него строку => "I love arrays they are my favorite"
lst2 = ["I", "love", "arrays", "they", "are", "my", "favorite"]
str_from_lst2 = ' '.join(lst2)
print('Task 3:', str_from_lst2)

# Task4: Создайте список из 10 элементов, вставьте на 3-ю позицию новое
# значение, удалите элемент из списка под индексом 6
lst3 = list(range(1, 11))
lst3[2] = 33
del lst3[6]
print('Task 4:', lst3)

# Task5: Есть 2 словаря
# a = { 'a': 1, 'b': 2, 'c': 3}
# b = { 'c': 3, 'd': 4,'e': “”}
# 1. Создайте словарь, который будет содержать в себе все элементы обоих
# словарей
# 2. Обновите словарь “a” элементами из словаря “b”
# 3. Проверить что все значения в словаре “a” не пустые либо не равны нулю
# 4. Проверить что есть хотя бы одно пустое значение (результат выполнения
# должен быть True)
# 5. Отсортировать словарь по алфавиту в обратном порядке
# 6. Изменить значение под одним из ключей и вывести все значения
a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': []}
ab = {**a, **b}
print('Task 5.1:', ab)
a.update(b)
print('Task 5.2:', a)
print('Task 5.3:', all(a.values()))
print('Task 5.4:', False if all(a.values()) is True else True)
sorting = dict(sorted(a.items(), reverse=True))
print('Task 5.5:', sorting)
sorting['e'] = 200
print('Task 5.6:', sorting)

# Task6: Создать список из элементов list_a = [1,2,3,4,4,5,2,1,8]
# 1. Вывести только уникальные значения и сохранить их в отдельную переменную
# 2. Добавить в полученный объект значение 22
# 3. Сделать list_a неизменяемым
# 4. Измерить его длинну
list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]
unique_a = list(set(list_a))
print('Task 6.1:', unique_a)
unique_a.append(22)
print('Task 6.2:', unique_a)
unchangeable_list = tuple(list_a)
print('Task 6.3:', unchangeable_list)
print('Task 6.4:', len(list_a))

# Задачи на закрепление форматирования (TaskF)
# TaskF1: Есть переменные a=10, b=25
# Вывести 'Summ is <сумма этих чисел> and diff = <их разница>.'
# При решении задачи использовать оба способа форматирования
a = 10
b = 25
print(f'Task F1: Summ is {a + b} and diff = {b - a}')

# TaskF2: Есть список list_of_children = [“Sasha”, “Vasia”, “Nikalai”]
# Вывести “First child is <первое имя из списка>, second is “<второе>”,
# and last one – “<третье>””
list_of_children = ['Sasha', 'Vasia', 'Nikalai']
print(f'Task F2: First child is {list_of_children[0]}, second is '
      f'{list_of_children[1]} and last one – {list_of_children[2]}')

# Task1*: *1) Вам передан массив чисел. Известно, что каждое число в этом
# массиве имеет пару, кроме одного: [1, 5, 2, 9, 2, 9, 1] => 5
# Напишите программу, которая будет выводить уникальное число
pairs = [1, 5, 2, 9, 2, 9, 1]
for i in pairs:
    if pairs.count(i) == 1:
        print('Task 1*:', i)

# Task2*: *2) Дан текст, который содержит различные английские буквы и знаки
# препинания. Вам необходимо найти самую частую букву в тексте. Результатом
# должна быть буква в нижнем регистре. При поиске самой частой буквы, регистр
# не имеет значения, так что при подсчете считайте, что "A" == "a". Убедитесь,
# что вы не считайте знаки препинания, цифры и пробелы, а только буквы. Если в
# тексте две и больше буквы с одинаковой частотой, тогда результатом будет
# буква, которая идет первой в алфавите. Для примера, "one" содержит "o", "n",
# "e" по одному разу, так что мы выбираем "e".
# "a-z" == "a"
# "Hello World!" == "l"
# "How do you do?" == "o"
# "One" == "e"
# "Oops!" == "o"
# "AAaooo!!!!" == "a"
# "a" * 9000 + "b" * 1000 == "a"
text = '"a" * 9000 + "b" * 1000'
text = text.lower()
text = ''.join(sorted(text))
only_letters = [x for x in text if x.isalpha() is True]
mostly = Counter(only_letters).most_common(1)
a, b = mostly[0]
print('Task 2*:', a)
