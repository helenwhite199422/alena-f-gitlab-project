# Задание 3
# На главной странице перейти по ссылке Add/Remove elements
# Проверить что при нажатии на кнопку Add element, появляется новый элемент
# Написать отдельный тест, который проверяет удаление элементов

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By

ELEMENTS_MENU_XPATH = '//a[text()="Add/Remove Elements"]'
ADD_ELEMENT_BUTTON = '[onclick="addElement()"]'
DELETE_BUTTON = '[class="added-manually"]'


def test_add_item(open_main_page):
    driver = open_main_page
    driver.find_element(By.XPATH, ELEMENTS_MENU_XPATH).click()
    driver.find_element(By.CSS_SELECTOR, ADD_ELEMENT_BUTTON).click()
    delete_buttons = driver.find_elements(By.CSS_SELECTOR, DELETE_BUTTON)
    assert delete_buttons != []


def test_delete_items(open_main_page):
    driver = open_main_page
    driver.find_element(By.XPATH, ELEMENTS_MENU_XPATH).click()
    search_add_element = driver.find_element(By.CSS_SELECTOR,
                                             ADD_ELEMENT_BUTTON)
    action = ActionChains(driver)
    action.double_click(on_element=search_add_element)
    action.perform()
    search_delete_buttons = driver.find_elements(By.CSS_SELECTOR,
                                                 DELETE_BUTTON)
    assert len(search_delete_buttons) == 2
    search_delete_buttons[1].click()
    search_delete_buttons[1].click()
    search_delete_buttons2 = driver.find_elements(By.CSS_SELECTOR,
                                                  DELETE_BUTTON)
    assert search_delete_buttons2 == []
