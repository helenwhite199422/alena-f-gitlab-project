from selenium import webdriver
from selenium.webdriver.common.by import By

LINK = 'https://the-internet.herokuapp.com/login'


def test_passed_login():
    try:
        driver = webdriver.Chrome()
        driver.get(LINK)
        element1 = driver.find_element(By.CSS_SELECTOR, "[type=text]")
        element1.send_keys("tomsmith")
        element2 = driver.find_element(By.ID, "password")
        element2.send_keys("SuperSecretPassword!")
        element3 = driver.find_element(By.CSS_SELECTOR, "[type=submit]")
        element3.submit()
        element4 = driver.find_element(By.CLASS_NAME, "subheader")
        res = element4.text
        assert res == 'Welcome to the Secure Area. When you are done click' \
                      ' logout below.'

    finally:
        driver.quit()


def test_failed_login():
    try:
        driver = webdriver.Chrome()
        driver.get(LINK)
        element1 = driver.find_element(By.CSS_SELECTOR, "[type=text]")
        element1.send_keys("tomsmith")
        element2 = driver.find_element(By.ID, "password")
        element2.send_keys("SuperSecretPasswordcsdcd")
        element3 = driver.find_element(By.CSS_SELECTOR, "[type=submit]")
        element3.submit()
        element4 = driver.find_element(By.ID, "flash")
        res = element4.text
        assert 'Your password is invalid!' in res
    finally:
        driver.quit()
