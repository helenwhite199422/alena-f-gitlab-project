import decorators
import math


# слова “Before” and “After”
@decorators.add_before_after
def func1():
    """This function prints It is function 1"""
    print('It is function 1')


@decorators.add_one
def func2(a):
    """The function returns a"""
    return a


@decorators.make_uppercase
def func3(a):
    """The function returns a"""
    return a


@decorators.func_name
def func4(x):
    """The function returns x"""
    return x


@decorators.change
def func5(*args, **kwargs):
    """The function prints arguments"""
    print(*args, kwargs)


@decorators.timer
def func6(x):
    """The function makes a calculation"""
    return (x ** 20) - x * 123


@decorators.timer
@decorators.func_name
def func7(n):
    """The function calculates n-number from Fibonacci Sequence and wrapped up
     by timer and func_name decorators"""
    first_elem = 0
    second_elem = 1
    im = n - 2
    while im > 0:
        next_elem = first_elem + second_elem
        first_elem = second_elem
        second_elem = next_elem
        im -= 1
    if n > 2:
        print(next_elem)
    elif n == 1:
        print(0)
    elif n == 2:
        print(1)


@decorators.add_before_after
@decorators.add_one
def func8():
    """This function makes a calculation and wrapped up
     by decorators from task 1, 2"""
    return math.sqrt(math.pi + 123456)


@decorators.typed(type='str')
def add_two_symbols(a, b):
    """The function returns sum of a and b"""
    return a + b


@decorators.typed(type='int')
def add_three_symbols(a, b, с):
    """The function returns sum of a, b and c"""
    return a + b + с
