from bank_deposit import bank
import unittest


class TestBank(unittest.TestCase):
    def test_bank_positive_deposit(self):
        answer = bank(100, 1)
        self.assertEqual(answer, 110.0)

    def test_bank_zero_deposit(self):
        answer = bank(0, 1)
        self.assertEqual(answer, 0.0)

    def test_bank_negative_deposit(self):
        answer = bank(-100, 1)
        self.assertNotEqual(answer, 110.0)

    @unittest.expectedFailure
    def test_bank_str_fail(self):
        answer = bank(100, "abc")
        self.assertEqual(answer, 123.4)

    @unittest.expectedFailure
    def test_bank_fail(self):
        answer = bank(100, 3)
        self.assertEqual(answer, 500.1)


if __name__ == '__main__':
    unittest.main()
