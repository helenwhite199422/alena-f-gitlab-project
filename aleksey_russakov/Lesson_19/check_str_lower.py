"""
проверяет что все буквы в переданном слове в нижнем регистре и возвращает True
либо False
"""


def check_lower(string: str) -> bool:
    return string.islower()


if __name__ == '__main__':
    print(check_lower("1"))
