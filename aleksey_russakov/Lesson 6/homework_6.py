# 1. Validate
# Ваша задача написать программу, принимающее число - номер кредитной карты(
# число может быть четным или не четным). И проверяющей может ли такая карта
# существовать. Предусмотреть защиту от ввода букв, пустой строки и т.д.

def card_method(card_number: int):
    number = card_number
    number *= 2
    if number > 9:
        number -= 9
    return number


def check_credit_card(cred_card: list):
    """
    Checks if your card is valid
    """
    card_check = cred_card
    for i in range(len(card_check)):
        # even
        if len(card_check) % 2 == 0:
            if i % 2 == 0:
                card_check[i] = card_method(card_check[i])
        # odd
        else:
            if i % 2 == 1:
                card_check[i] = card_method(card_check[i])
    if sum(card_check) % 10 == 0:
        print("Your card is valid")
    else:
        print("Your card is invalid")


credit_card = input("Input your credit card number: ")
# Проверка на ввод данных
while credit_card.isdigit() is False or not credit_card:
    credit_card = input("Input correct credit card number: ")
credit_card = [int(i) for i in credit_card]
check_credit_card(credit_card)


# 2. Подсчет количества букв
# На вход подается строка, например, "cccbba" результат работы программы -
# строка “c3b2a"

def letter_count(input_str: str):
    """
    This function counts the similar letters and returns a string with
    that result
    """
    new_string = []
    for letter in input_str:
        letter_number_str = letter + str(input_str.count(letter))
        if letter_number_str not in new_string:
            new_string.append(letter_number_str)
    new_string = ''.join(new_string)
    return new_string


second_str = input("Input your string: ")
print(letter_count(second_str))


# 3. Простейший калькулятор v0.1
# Реализуйте программу, которая спрашивала у пользователя, какую операцию он
# хочет произвести над числами, а затем запрашивает два числа и выводит
# результат
# Проверка деления на 0.

def calculator(input_operation: str, input_number_1: float,
               input_number_2: float):
    """
    Simple calculator
    """
    if input_operation == '+':
        return input_number_1 + input_number_2
    if input_operation == '-':
        return input_number_1 - input_number_2
    if input_operation == '*':
        return input_number_1 * input_number_2
    if input_operation == '/':
        if input_number_1 == 0:
            return 'Inf.'
        return input_number_1 / input_number_2


operation = ""
operations = ['+', '-', '*', '/']
while operation not in operations:
    operation = input('Input the operation (use +, -, *, /) : ')

numbers = [""] * 2
for i in range(len(numbers)):
    while not numbers[i].isdigit():
        numbers[i] = input(f'Enter the number {i + 1}: ')
print(calculator(operation, float(numbers[0]), float(numbers[1])))


# 4. Написать функцию с изменяемым числом входных параметров
# При объявлении функции предусмотреть один позиционный и один именованный
# аргумент, который по умолчанию равен None (в примере это аргумент с именем
# name).
# Также предусмотреть возможность передачи нескольких именованных и
# позиционных аргументов.
# Функция должна возвращать следующее
# result = function(1, 2, 3, name=’test’, surname=’test2’, some=’something’)
# Print(result)
# 🡪 {“mandatory_position_argument”: 1, “additional_position_arguments”: (2, 3),
#      “mandatory_named_argument”: {“name”: “test2”},
#      “additional_named_arguments”:       {“surname”: “test2”, “some”:
#      “something”}}

def arg_func(x, *args, name=None, **kwargs):
    """
    Function than returns argument's commentary.
    """
    return {'mandatory_position_argument': x,
            'mandatory_named_argument': {'name': name},
            'additional_position_arguments': args,
            'additional_named_arguments': {kwargs}
            }


result = arg_func(1, 2, 3, name='test', surname='test2', some='something')
print(result)


# 5. Работа с областями видимости
# На уровне модуля создать список из 3-х элементов
# Написать функцию, которая принимает на вход этот список и добавляет в него
# элементы. Функция должна вернуть измененный список.
# При этом исходный список не должен измениться. Пример c функцией которая
# добавляет в список символ “a”:
# My_list = [1, 2, 3]
# Changed_list = change_list(My_list)
# Print(My_list)
# 🡪 [1, 2, 3]
# Print(Changed_list)
# 🡪 [1, 2, 3, “a”]

def change_list(my_lst: list):
    """
    This function adds "a" to the end of the input list
    """
    lst = my_lst
    lst.append('a')
    return lst


list_5 = [i for i in range(3)]
print('Initial list: ', list_5)
changed_list = change_list(list_5)
print('New list: ', changed_list)


# 6. Функция проверяющая тип данных
# Написать функцию которая принимает на фход список из чисел, строк и таплов
# Функция должна вернуть сколько в списке элементов приведенных данных
# print(my_function([1, 2, “a”, (1, 2), “b”])
# 🡪 {“int”: 2, “str”: 2, “tuple”: 1}

def count_type_func(input_list: list):
    """
    This function takes a list and shows count of types.
    """
    int_count = 0
    str_count = 0
    tuple_count = 0
    for i in range(len(input_list)):

        if type(input_list[i]) == int:
            int_count += 1
        if type(input_list[i]) == str:
            str_count += 1
        if type(input_list[i]) == tuple:
            tuple_count += 1
    type_dict = {'int': int_count, 'str': str_count, 'tuple': tuple_count}
    return type_dict


list_6 = [1, 2, 'a', (1, 2), 'b']
print(count_type_func(list_6))

# 7. Написать пример чтобы hash от объекта 1 и 2 были одинаковые, а id разные.

obj_1 = 1
obj_2 = 1
print(f'id: {id(obj_1)}, {id(obj_2)}')
print(f'hash : {hash(obj_1)}, {hash(obj_2)}')


# 8. написать функцию, которая проверяет есть ли в списке объекты, которые
# можно вызвать

def last_func(input_list: list):
    """
    This function checks the list and shows another list of bool values of
    each element to be callable.
    """
    return [callable(i) for i in input_list]


lil_list = [1, 2, 3]
my_list = [0, lil_list, 'a', last_func]
print(last_func(my_list))
