from selenium.webdriver.common.by import By


class BasketPageLocators:
    EMPTY_INDICATOR = (By.XPATH, '//*[@id="content_inner"]')
    BASKET_HEADER = (By.XPATH, '//h1')
    VIEW_BASKET = (By.XPATH,
                   '/ html / body / header / div[1] / div / div[2] / span / a')
