"""
Суть задания:
На ферме могут жить животные и птицы
И у тех и у других есть два атрибута:
 имя и возраст
При этом нужно сделать так, чтобы возраст
 нельзя было изменить извне напрямую,
но при этом можно было бы получить его значение
У каждого животного должен быть обязательный метод – ходить
А для птиц – летать
Эти методы должны выводить сообщения (контекст придумайте)
Также должны быть общие для всех классов
обязательные методы – постареть -
который увеличивает возраст на 1 и метод голос

При обращении к несуществующему методу
 или атрибуту животного или птицы должен
срабатывать пользовательский exception NotExistException
и выводить сообщение о том, что для текущего класса (имя класса),
такой метод или атрибут не существует – (имя метода или атрибута)

На основании этих классов создать
 отдельные классы для Свиньи, Гуся,
Курицы и Коровы – для каждого класса
 метод голос должен выводить разное
сообщение (хрю, гага, кудах, му)

После этого создать класс Ферма,
в который можно передавать список животных
Должна быть возможность получить
 каждое животное, живущее на ферме как
по индексу, так и через цикл for.
Также у фермы должен быть метод,
 который увеличивает возраст всех животных,
живущих на ней.
"""

from abc import ABC, abstractmethod


class Animal(ABC):
    @abstractmethod
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __getattr__(self, name):
        raise NotExistException(f'{self.__class__.__name__}'
                                f' not attribute {name}')

    @abstractmethod
    def old(self):
        print(self.age + 1)

    @abstractmethod
    def move_to(self):
        print("move")

    @abstractmethod
    def voice(self):
        print("voice")


class Bird(ABC):
    @abstractmethod
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __getattr__(self, name):
        raise NotExistException(f'{self.__class__.__name__}'
                                f' no attribute {name}')

    @abstractmethod
    def old(self):
        print(self.age + 1)

    @abstractmethod
    def fly(self):
        print("fly to ....")

    @abstractmethod
    def voice(self):
        print("voice for ..")


class Pigs(Animal):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def old(self):
        print(self.age + 1)

    def move_to(self):
        print("step to ...")

    def voice(self):
        print("hru....")


class Cows(Animal):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def old(self):
        print(self.age + 1)

    def move_to(self):
        print("step to....")

    def voice(self):
        print("Myy.....")


class Goose(Bird):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def old(self):
        print(self.age + 1)

    def fly(self):
        print("fly to....")

    def voice(self):
        print("gaga....")


class Chicken(Bird):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def old(self):
        print(self.age + 1)

    def fly(self):
        print("fly to....")

    def voice(self):
        print("ky dah ....")


class Farm:
    def __init__(self, *args):
        self.args = args

    def __getitem__(self, index):
        return self.args[index]

    def __iter__(self):
        return iter(self.args)

    @staticmethod
    def older(self, *args):
        for beast in args:
            beast.age += 3
            return self


pig_1 = Pigs("pigs", 47)
pig_1.voice()
pig_1.old()
pig_1.move()

cow_1 = Cows("cows", 33)
cow_1.voice()
cow_1.old()
cow_1.move()

goose_1 = Goose("goose", 21)
goose_1.voice()
goose_1.old()
goose_1.fly()

chicken_1 = Chicken("chicken", 7)
chicken_1.voice()
chicken_1.old()
chicken_1.fly()

farm = Farm(pig_1, cow_1, goose_1, chicken_1)
print(pig_1.name)
print(cow_1.name)
print(goose_1.name)
print(chicken_1.name)
for f in farm:
    print(f.name)
farm.older(pig_1, cow_1, goose_1, chicken_1)
print("Pig age full ", pig_1.age)


class NotExistException(Exception):
    pass


pig_1.color()
