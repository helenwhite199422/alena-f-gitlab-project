# 1. Assign values to variables.
# 2. Get the arithmetic mean.
# 3. Get the geometric mean.
a, b = 2, 6
ari_average = (a + b) / 2
geo_average = (a * b) ** 0.5

print(ari_average)
print(geo_average)
