import unittest
from unittest import TestCase
from vitalii_bilyk.lecture_18.calculator import\
    find_sum, find_diff, find_multiple, find_div


class TestCheckCalculator(TestCase):

    def test_find_sum_positive_numbers_ok(self):
        self.assertEqual(find_sum(2, 4), 6)

    def test_find_sum_positive_plus_negative_ok(self):
        self.assertEqual(find_sum(-3, 5), 2)

    @unittest.expectedFailure
    def test_find_sum_negative_numbers_fail(self):
        self.assertNotEqual(find_sum(-130, -50), -180)

    def test_find_diff_positive_numbers_ok(self):
        self.assertEqual(find_diff(1000, 100), 900)

    def test_find_diff_negative_numbers_ok(self):
        self.assertEqual(find_diff(-9, -6), -3)

    @unittest.expectedFailure
    def test_find_diff_positive_numbers_fail(self):
        self.assertNotEqual(find_diff(1000, 1200), -200)

    def test_find_multiple_positive_numbers_ok(self):
        self.assertEqual(find_multiple(8, 8), 64)

    def test_find_multiple_positive_negative_ok(self):
        self.assertEqual(find_multiple(-16, 100), -1600)

    @unittest.expectedFailure
    def test_find_multiple_negative_numbers_fail(self):
        self.assertNotEqual(find_multiple(-8, -8), 64)

    def test_find_div_positive_numbers_ok(self):
        self.assertEqual(find_div(0, 8), 0)

    def test_find_div_negative_numbers_ok(self):
        self.assertEqual(find_div(-8, -2), 4)

    def test_find_div_positive_by_zero(self):
        with self.assertRaises(ZeroDivisionError):
            find_div(5, 0)
