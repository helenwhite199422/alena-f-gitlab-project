import pytest
import datetime


@pytest.fixture(scope='module', autouse=True)
def show_module(request):
    module = request.node.name
    print('Current module:', module)


@pytest.fixture(scope='session', autouse=True)
def tests_time():
    start = datetime.datetime.now()
    print('\nSession started:', start)
    yield start
    print('\nFinished:', datetime.datetime.now())


@pytest.fixture(autouse=True)
def show_fixtures(request):
    module = request.fixturenames
    print('\nFixtures used:', module)


@pytest.fixture(autouse=True)
def time_since_session_start(tests_time):
    current_time = datetime.datetime.now()
    result = current_time - tests_time
    print(result, 'since session start')
