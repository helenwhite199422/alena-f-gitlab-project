# 1. Перевести строку в массив.
s = "Robin Singh".split()
s2 = "I love arrays they are my favorite".split()
print(s, s2, sep='\n')

# 2. Напечатать текст “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”.
my_list = ['Ivan', 'Ivanou']
s = 'Minsk'
s2 = 'Belarus'
print(f'Привет, {my_list[0]} {my_list[1]}!'
      f' Добро пожаловать в {s} {s2}')

# 3. Сделать из списка строку.
my_list = ["I", "love", "arrays", "they", "are", "my", "favorite"]
print(' '.join(my_list))

# 4. Создать список из 10 элементов,
# вставить на 3-ю позицию новое значение,
# удалить элемент из списка под индексом 6.
my_list = [i for i in range(10)]
my_list[2] = 100
del my_list[6]
print(my_list)

# 5.
a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ''}

# Создать словарь, содержащий все элементы обоих словарей.
c = {**a, **b}
print(c)

# Обновите словарь “a” элементами из словаря “b”.
a.update(b)
print(a)

# Проверить, что все значения в словаре "а" не пустые либо не равны нулю.
print(all(a.values()))

# Проверить, что есть хотя бы одно пустое значение.
print('' in a.values())

# Отсортировать словарь по алфавиту в обратном порядке.
print(dict(sorted(a.items(), reverse=True)))

# Изменить значение под одним из ключей и вывести все значения.
b['d'] = 3
print(b)

# 6.
list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]

# Вывести только уникальные значения и сохранить в отдельную переменную.
b = [k for k, i in enumerate(list_a) if list_a.index(k) == i]
print(b)

# Добавить в полученный объект значение '22'.
b.append(22)
print(b)

# Сделать list_a неизменяемым.
# Измерить его длину.
list_a = tuple(list_a)
print(len(list_a))

# Задачи на закрепление форматирования.
# 1. Вывести 'Summ is <сумма этих чисел> and diff = <их разница>.'
# Оба способа форматирования.
a = 10
b = 25
print(f'Summ is {a + b} and diff = {a - b}.')
print('Summ is {} and diff = {}.'.format(a + b, a - b))

# 2.
list_of_children = ['Sasha', 'Vasia', 'Nikalai']

# Вывести "First child is <первое имя из списка>,
# second is '<второе>', and last one – '<третье>'"
print('First child is {},'
      ' second is {},'
      ' and last one – {}'.format(list_of_children[0],
                                  list_of_children[1],
                                  list_of_children[2]))
