import pytest
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


def test_checkboxes(open_main_page):
    driver = open_main_page
    driver.find_element(By.CSS_SELECTOR, 'a[href="/checkboxes"]').click()
    checkbox_2 = driver.find_element(
        By.XPATH, '//*[@id="checkboxes"]/input[2]')
    checkbox_2.click()
    checkbox_1 = driver.find_element(
        By.XPATH, '//*[@id="checkboxes"]/input[1]')
    checkbox_1.click()
    assert checkbox_2.is_selected(), "Second checkbox is not checked."
    assert not checkbox_1.is_selected(), "First checkbox is checked."


def test_multiple_windows(open_main_page):
    driver = open_main_page
    driver.find_element(By.LINK_TEXT, "Multiple Windows").click()
    driver.find_element(By.LINK_TEXT, "Click Here").click()
    driver.switch_to.window(driver.window_handles[1])
    assert driver.title == "New Window"


def test_add_element_button(open_main_page):
    driver = open_main_page
    driver.find_element(By.LINK_TEXT, "Add/Remove Elements").click()
    driver.find_element(By.CSS_SELECTOR, ".example > button").click()
    delete_button = driver.find_element(
        By.CSS_SELECTOR, '#elements > button')

    assert delete_button.is_displayed


def test_delete_button(open_main_page):
    driver = open_main_page
    driver.find_element(By.LINK_TEXT, "Add/Remove Elements").click()
    driver.find_element(By.CSS_SELECTOR, ".example > button").click()
    delete_button = driver.find_element(By.CSS_SELECTOR, '#elements > button')
    delete_button.click()
    with pytest.raises(NoSuchElementException):
        driver.find_element(By.CSS_SELECTOR, '#elements > button')
