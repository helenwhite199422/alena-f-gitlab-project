from pages.books_page import BooksPage


def test_open_book(driver):
    book_title = 'Hacking Exposed Wireless'

    page = BooksPage(driver)
    page.open_page()
    book_page = page.open_book_detail_page(book_title)

    assert book_title in book_page.get_page_title(), "Incorrect page title"
    assert book_page.get_book_name() == 'Hacking Exposed Wireless',\
        "Incorrect book has been opened"
    assert book_page.get_image(), "Image is not visible"
