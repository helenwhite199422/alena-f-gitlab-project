from selenium.webdriver.common.by import By
from pages.base_page import BasePage
from pages.book_detail_page import BookDetailPage


class BooksPage(BasePage):
    URL = 'http://selenium1py.pythonanywhere.com/en-gb/catalogue/category/' \
          'books_2/'

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def open_book_detail_page(self, title):
        book = self.driver.find_element(By.LINK_TEXT, title)
        book.click()
        return BookDetailPage(self.driver, self.driver.current_url)
